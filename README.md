# HCI-Mobile



## Getting started
This repository is a compilation of resources to easier your task in developing a mobile application using a web-based approach. We have created 2 main subprojects that contain some templates for you to use freely in your final task, namely:

- Back-end 
- Front-end 

We also offer a set of videos to illustrate how to use this repo: [youtube](https://youtube.com/playlist?list=PLa0X8b3FvpXNY0AfME-J9s1sDhE9qLFcl)

<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLa0X8b3FvpXNY0AfME-J9s1sDhE9qLFcl" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Back-end
Contains the basic template to start building the routes(endpoints) and controllers needed to  establish a connection with a datatbase and make some [CRUD](https://www.freecodecamp.org/news/crud-operations-explained/) operations. 

To execute this subproject simple navigate to the host folder and run the command `nodemon` after installing the requiered dependecies(`npm install`).

Back-end template is build in Node.js and it is powered with [Express.js](https://expressjs.com/). The database is handled usign [MongoDB](https://www.mongodb.com/atlas/database).




## Front-end
This subprojects contains two solutions:

### react-template 
Is a **development** template to design interfaces using [ReactJS](https://reactjs.org/).
To start the project we need to install the required dependecies in the same way as we did in the back-end project. After installing the dependecies the react application runs using the command `npm start`. 

Altenatively you could use [Codepen](https://codepen.io/topic/react), [Codesanbox](https://codesandbox.io/) or [Playcode](playcode.io/react) to create your designs.

*Note that at the end of developement process is needed to build your project using the command `npm run build`. The generate files will need to be served as static files in the back-end.*

### android-template 
Contains the a demo project that consumes the endpoint of our back-end project and displays the results in a custom ListView. 

## Some other resources

- [FigmaToCode](https://www.figma.com/community/plugin/1037309320238203168/DhiWise---Figma-to-Code)
- [freemysqlhosting](https://www.freemysqlhosting.net/)
- [db4free](https://www.db4free.net/)
- [web development](https://gitlab.com/famnit-up/systems-iii/frameworks/)

